<?php get_header(); ?>
<form id="order" method="POST" action="<?php the_permalink(); ?>">
<h1>Оформление заказа</h1>
<div class="center">
<ul class="tabs">
    <?php for ($i = 1; $i <= 8; $i++): ?>
    <li>
        <a href="#">
            <div class="inputfield" >
                <label><?php echo synved_option_get('options', 'row'.$i.'label') ?></label>
                <div class="bgactive">
                    <input type="text" class="inpfield required" name="<?php echo 'row'.$i ?>" data-label="<?php echo synved_option_get('options', 'row'.$i.'label') ?>" value=""/>
                    <span class="b1"></span><span id="bg<?php echo $i ?>"></span></div>
                <div class="clear"></div>
            </div>
        </a>
    </li>
    <?php endfor; ?>
</ul>
<div class="panes">
<?php for ($i = 1; $i <= 8; $i++): ?>
<div>
    <div class="block-video">
        <div class="block-video-title"><span><?php echo synved_option_get('options', 'row'.$i.'header') ?></span></div>
        <div class="video">
            <iframe id="player" type="text/html" width="492" height="300"
                    src="<?php echo synved_option_get('options', 'row'.$i.'video') ?>"
                    frameborder="0"></iframe>
        </div>
    </div>
</div>
<?php endfor; ?>
</div>

<div class="price">Цена: <?php echo synved_option_get('options', 'price') ?> грн</div>
<div class="order-form">

        <div class="form-row">
            <input placeholder="Имя" class="required" type="text" name="name" data-label="Имя">
            <input placeholder="Телефон" class="required" type="text" name="phone" data-label="Телефон">
            <input placeholder="Email" class="required" type="text" name="email" data-label="Email">
        </div>
        <div class="form-row">
            <textarea class="textarea" name="comments" data-label="Комментарии">комментарии к заказу...</textarea>
            <div class="clear"></div>
        </div>
        <div class="form-row">
            <p>Прочитайте наши <a class="conditions" href="/conditions/">условия</a> работы</p>
            <input name="conditions" type="checkbox" class="required" data-label="Условия"/>
            <label>Я принимаю условия работы</label>
            <div class="clear"></div>
        </div>
        <div class="form-row-submit">
            <ul id="errors">
            </ul>
            <input type="submit" value="Оформить заказ"/>
            <div class="clear"></div>
        </div>

</div>

</div>
<div class="sidebar">
    <fieldset>
        <legend align="left">Модель</legend>
        <a href="/models/">
            <?php if(isset($_SESSION['model']['title'])): ?>
                <input type="hidden" class="required" name="model" value="<?php echo $_SESSION['model']['title'] ?>">
                <?php echo $_SESSION['model']['title'] ?>
            <?php else: ?>
                <input type="hidden" class="required" name="model" value="">
                Выбрать модель
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Тип посадки</legend>
        <p><input type="radio" class="required" name="r1" value="Высокая" data-label="Тип посадки"/><label>Высокая</label></p>
        <p><input type="radio" name="r1" value="Средняя" data-label="Тип посадки"/><label>Средняя</label></p>
        <p><input type="radio" name="r1" value="Низкая" data-label="Тип посадки"/><label>Низкая</label></p>
    </fieldset>
    <fieldset>
        <legend align="left">Ткань</legend>
        <a href="/cloth/">
            <?php if(isset($_SESSION['cloth']['title'])): ?>
            <input type="hidden" class="required" name="cloth" data-label="Ткань" value="<?php echo $_SESSION['cloth']['title'] ?>">
            <?php echo $_SESSION['cloth']['title'] ?>
            <?php else: ?>
            <input type="hidden" class="required" name="cloth" data-label="Ткань" value="">
            Выбрать ткань
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Нить</legend>
        <a href="/thread/">
            <?php if(isset($_SESSION['thread']['title'])): ?>
            <input type="hidden" name="thread" data-label="Нить" value="<?php echo $_SESSION['thread']['title'] ?>">
            <?php echo $_SESSION['thread']['title'] ?>
            <?php else: ?>
            <input type="hidden" class="required" name="thread" data-label="Нить" value="">
            Выбрать нить
            <?php endif; ?>
        </a>
    </fieldset>
    <fieldset>
        <legend align="left">Механические изменения</legend>
        <p><input type="radio" class="required" name="r2" value="Базовые" data-label="Механические изменения"/><label>Базовые</label></p>
        <p><input type="radio" name="r2" value="Локальные" data-label="Механические изменения"/><label>Локальные</label></p>
    </fieldset>
</div>
</form>
<?php get_footer() ?>