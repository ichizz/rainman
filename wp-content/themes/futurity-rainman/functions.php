<?php
function formsubmit() {
    $message = "<h1>Заказ на джинсы Rain Man</h1><br>
    <br>
    Имя: {$_POST['name']}<br>
    Email: {$_POST['email']}<br>
    Телефон: {$_POST['phone']}<br>
    Комментарий:<br>
    {$_POST['comments']}<br>
    <br>
    <b>Параметры заказа:</b><br>
    Модель: {$_POST['model']}<br>
    Тип посадки: {$_POST['r1']}<br>
    Ткань: {$_POST['cloth']}<br>
    Нить: {$_POST['thread']}<br>
    Механические изменения: {$_POST['r2']}<br>
    <br>
    <b>Размеры:</b><br>";

    for ($i=1; $i<=8; $i++) {
        $message .= synved_option_get('options', 'row'.$i.'label');
        $message .= ": ".$_POST['row'.$i]."<br>";
    }

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= 'From: Rain Man <order@rain-man.com.ua>' . "\r\n";

    mail($_POST['email'], 'Rain Man: Новый заказ', $message, $headers);
    mail(synved_option_get('options', 'email'), 'Rain Man: Новый заказ', $message, $headers);

    $_SESSION = '';

    header('location:/thankyou/');
    die;
}

function rainmain_setup() {
    session_start();
    if (isset($_POST['name'])) {
        formsubmit();
    }

    load_theme_textdomain( 'rainman', get_template_directory() . '/languages' );
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 236, 246, true );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
    register_nav_menu( 'primary', __( 'Главное меню', 'rainman' ) );

    $options = array(
        'left' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Слева', 'rainman')
        ),
        'right' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Справа', 'rainman')
        ),
        'vkid' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('VK API ID', 'rainman')
        ),
        'email' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Email для заказов', 'rainman')
        ),
        'price' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Цена', 'rainman')
        ),
        'vk' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('ВКонтакте', 'rainman')
        ),
        'odnoklasniki' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Одноклассники', 'rainman')
        ),
        'facebook' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Facebook', 'rainman')
        ),
        'twitter' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Twitter', 'rainman')
        ),
        'instagram' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Instagram', 'rainman')
        ),
        'google' => array(
            'default' => '',
            'type' => 'text',
            'label' => __('Google+', 'rainman')
        ),
    );

    for ($i = 1; $i <= 8; $i++) {
        $options['row'.$i.'label'] = array(
            'default' => '',
            'type' => 'text',
            'label' => __('Параметр '.$i.'. Название', 'rainman')
        );
        $options['row'.$i.'header'] = array(
            'default' => '',
            'type' => 'text',
            'label' => __('Параметр '.$i.'. Заголовок', 'rainman')
        );
        $options['row'.$i.'video'] = array(
            'default' => '',
            'type' => 'text',
            'label' => __('Параметр '.$i.'. Видео', 'rainman')
        );
    }

    synved_option_register('options', $options);
}

add_action( 'after_setup_theme', 'rainmain_setup' );