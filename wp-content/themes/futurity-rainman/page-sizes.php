<?php get_header(); ?>
    <form id="order" method="POST" action="<?php the_permalink(); ?>">
        <h1>Оформление заказа</h1>
        <div class="center">
            <ul class="nav-sizes">
                <li>
                    <p>
                        Размер
                    </p>
                </li>
                <li>|</li>
                <li>
                    <a href="#">По мерке</a>
                </li>
            </ul>
            <ul class="tabs">
                <li>
                    <div class="sizes">
                       <p>W29.L30</p>
                    </div>
                </li>
            </ul>
            <div class="rightarrow">
                <img src="http://rainman-wp/wp-content/themes/futurity-rainman/images/icons/sizes.png" alt=""/>
            </div>
            <div class="box-sizes">
                <table class="box-head">
                    <tr>
                        <td>
                            <p>US sizes</p>
                        </td>
                        <td>
                            <a href="#">US sizes</a>
                        </td>
                        <td>
                            <a href="#">EU sizes</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>0-20</p>
                        </td>
                        <td>
                            <p>23-32</p>
                        </td>
                        <td>
                            <p>W/L</p>
                        </td>
                    </tr>
                </table>
                <table class="box-body">
                    <thead>
                        <tr>
                            <td><p>Размер</p></td>
                            <td><p>Hip</p></td>
                            <td><p>Бедро</p></td>
                            <td><p>Inseam</p></td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><p>00</p></td>
                            <td><p>53</p></td>
                            <td><p>20.4</p></td>
                            <td><p>Extra short</p></td>
                        </tr>
                        <tr>
                            <td><p>0</p></td>
                            <td><p>36</p></td>
                            <td><p>20.9</p></td>
                            <td><p>28</p></td>
                        </tr>
                        <tr>
                            <td><p>2</p></td>
                            <td><p>37</p></td>
                            <td><p>2.4</p></td>
                            <td><p>Short</p></td>
                        </tr>
                        <tr>
                            <td><p>4</p></td>
                            <td><p>38</p></td>
                            <td><p>21.9</p></td>
                            <td><p>30&quot;</p></td>
                        </tr>
                        <tr>
                            <td><p>6</p></td>
                            <td><p>39</p></td>
                            <td><p>22.4</p></td>
                            <td><p>Medium</p></td>
                        </tr>
                        <tr>
                            <td><p>8</p></td>
                            <td><p>40</p></td>
                            <td><p>22.9</p></td>
                            <td><p>32&quot;</p></td>
                        </tr>
                        <tr>
                            <td><p>10</p></td>
                            <td><p>41</p></td>
                            <td><p>23.4</p></td>
                            <td><p>Long</p></td>
                        </tr>
                        <tr>
                            <td><p>12</p></td>
                            <td><p>41</p></td>
                            <td><p>23.9</p></td>
                            <td><p>34&quot;</p></td>
                        </tr>
                        <tr>
                            <td><p>14</p></td>
                            <td><p>43</p></td>
                            <td><p>24.4</p></td>
                            <td><p>Extra long</p></td>
                        </tr>
                        <tr>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p>36&quot;</p></td>
                        </tr>
                        <tr>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p>Super long</p></td>
                        </tr>
                        <tr>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p></p></td>
                            <td><p>38&quot;</p></td>
                        </tr>
                    </tbody>
                </table>
                <div class="tbl-sizes">
                    <input type="checkbox" id="tbl_sizes"/>
                    <label for="tbl_sizes">Таблица размеров</label>
                </div>
            </div>


            <div class="price">Цена: <?php echo synved_option_get('options', 'price') ?> грн</div>
            <div class="order-form">

                <div class="form-row">
                    <input placeholder="Имя" class="required" type="text" name="name" data-label="Имя">
                    <input placeholder="Телефон" class="required" type="text" name="phone" data-label="Телефон">
                    <input placeholder="Email" class="required" type="text" name="email" data-label="Email">
                </div>
                <div class="form-row">
                    <textarea class="textarea" name="comments" data-label="Комментарии">комментарии к заказу...</textarea>
                    <div class="clear"></div>
                </div>
                <div class="form-row">
                    <p>Прочитайте наши <a class="conditions" href="/conditions/">условия</a> работы</p>
                    <input name="conditions" type="checkbox" class="required" data-label="Условия"/>
                    <label>Я принимаю условия работы</label>
                    <div class="clear"></div>
                </div>
                <div class="form-row-submit">
                    <ul id="errors">
                    </ul>
                    <input type="submit" value="Оформить заказ"/>
                    <div class="clear"></div>
                </div>

            </div>

        </div>
        <div class="sidebar">
            <fieldset>
                <legend align="left">Модель</legend>
                <a href="/models/">
                    <?php if(isset($_SESSION['model']['title'])): ?>
                        <input type="hidden" class="required" name="model" value="<?php echo $_SESSION['model']['title'] ?>">
                        <?php echo $_SESSION['model']['title'] ?>
                    <?php else: ?>
                        <input type="hidden" class="required" name="model" value="">
                        Выбрать модель
                    <?php endif; ?>
                </a>
            </fieldset>
            <fieldset>
                <legend align="left">Тип посадки</legend>
                <p><input type="radio" class="required" name="r1" value="Высокая" data-label="Тип посадки"/><label>Высокая</label></p>
                <p><input type="radio" name="r1" value="Средняя" data-label="Тип посадки"/><label>Средняя</label></p>
                <p><input type="radio" name="r1" value="Низкая" data-label="Тип посадки"/><label>Низкая</label></p>
            </fieldset>
            <fieldset>
                <legend align="left">Ткань</legend>
                <a href="/cloth/">
                    <?php if(isset($_SESSION['cloth']['title'])): ?>
                        <input type="hidden" class="required" name="cloth" data-label="Ткань" value="<?php echo $_SESSION['cloth']['title'] ?>">
                        <?php echo $_SESSION['cloth']['title'] ?>
                    <?php else: ?>
                        <input type="hidden" class="required" name="cloth" data-label="Ткань" value="">
                        Выбрать ткань
                    <?php endif; ?>
                </a>
            </fieldset>
            <fieldset>
                <legend align="left">Нить</legend>
                <a href="/thread/">
                    <?php if(isset($_SESSION['thread']['title'])): ?>
                        <input type="hidden" name="thread" data-label="Нить" value="<?php echo $_SESSION['thread']['title'] ?>">
                        <?php echo $_SESSION['thread']['title'] ?>
                    <?php else: ?>
                        <input type="hidden" class="required" name="thread" data-label="Нить" value="">
                        Выбрать нить
                    <?php endif; ?>
                </a>
            </fieldset>
            <fieldset>
                <legend align="left">Механические изменения</legend>
                <p><input type="radio" class="required" name="r2" value="Базовые" data-label="Механические изменения"/><label>Базовые</label></p>
                <p><input type="radio" name="r2" value="Локальные" data-label="Механические изменения"/><label>Локальные</label></p>
            </fieldset>
        </div>
    </form>
<?php get_footer() ?>